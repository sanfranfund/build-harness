#!/bin/sh

##
# Script para login ECR utilizando chaves de api geradas pela AWS utilizando environments
# Provider: AWS
# Author: DevOps team Arena
# Dependencies: awscli
##

set -e

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_SESSION_TOKEN
export AWS_REGION

msg_error="Please set variable"

required_check_variables() {
    if [ -z "$AWS_ACCESS_KEY_ID" ]; then
        echo "$msg_error \$AWS_ACCESS_KEY_ID"
        exit
    fi

    if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
        echo "$msg_error \$AWS_SECRET_ACCESS_KEY"
        exit
    fi

    if [ -z "$AWS_REGION" ]; then
        echo "$msg_error \$AWS_REGION"
        exit
    fi

    return
}

command_exists () {
    type "$1" > /dev/null ;
}

ecr_login(){
    if command_exists aws ; then
        AWS_ECR_GET_LOGIN="aws ecr get-login --region ${AWS_REGION} --no-include-email"
        $AWS_ECR_GET_LOGIN
    else
        echo "No awscli"
    fi
}


main(){
    if required_check_variables; then
        ecr_login
    fi

    exit
}

main
