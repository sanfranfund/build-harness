#!/bin/bash

set -u

export ES_USER=${ES_USER:-elastic}
export ES_PASS=${ES_PASS:-elastic}
export ES_HOST=${ES_HOST:-elastic}
export GIT_BRANCH=${GIT_BRANCH:-localhost}
export GIT_COMMIT=${GIT_COMMIT:-localhost}

command_exists () {
    type "$1" > /dev/null;
    return
}

help(){
    what_help="$1"
    if [ $what_help ==  required ]; then
        echo -e "===> [ERROR] You need Terraform tool.\n
        ====> Get download in https://www.terraform.io/downloads.html.
        "
    fi

    if [ $what_help == terraform_validate ]; then
        echo -e "[ERROR] Usage path to files. Example: \n
         ===> $0 path_tf_files"
    fi

    # Usage: terraform_test main.tf
    if [ $what_help == usage ]; then
        echo -e "[ERROR] Not find required environment variables ELASTIC_USER ELASTIC_PASS ELASTIC_HOST TIER."
    fi
}

required(){
    echo "===> Checando terraform:"
    if command_exists terraform; then
        echo "> $(terraform version)"
        export TERRAFORM=$(which terraform)
        export TF_VERSION=`$TERRAFORM version | grep -i "Terraform" | head -n 1`
    else
        echo "> Inicializando terraform."
        GET_TERRAFORM_BIN=$(curl -s -S -O https://releases.hashicorp.com/terraform/0.12.7/terraform_0.12.7_linux_amd64.zip && unzip -o terraform_0.12.7_linux_amd64.zip -d /usr/local/bin && chmod +x /usr/local/bin/terraform)
        export TERRAFORM=$(which terraform)
        export TF_VERSION=`$TERRAFORM version | grep -i "Terraform" | head -n 1`
    fi
}

terraform_validate(){
    $TERRAFORM init -no-color -backend=false $1
    $TERRAFORM validate -no-color -json $1
    export check_error=`echo $?`
}

elasticsearch_log(){
    terraform_output=$(<$1)
    now=$(($(date +'%s * 1000 + %-N / 1000000'))) && \
    content_data_log=`echo ' {
        "@timestamp": "'"${now}"'" ,
        "branch": "'"${GIT_BRANCH}"'" ,
        "commit": "'"${GIT_COMMIT}"'" ,
        "terraform": "'"${TF_VERSION}"'"
      } ' ''"${terraform_output}"'' | \
      jq --slurp 'reduce .[] as $item ({}; . * $item)'`

    input_log=`curl --connect-timeout 60 -sS -H "Content-Type: application/json" \
    -X POST "https://${ES_USER}:${ES_PASS}@${ES_HOST}/deploys-terraform/_doc" \
    -d "$content_data_log"`
}

input_elasticsearch_log(){
    echo "===> Inserindo logs no Elasticsearch:"
    if ! elasticsearch_log $1; then
        echo "[WARN] Nao foi possivel realizar o insert do log no Elasticsearch"
    else
        echo "Logs inseridos com sucesso"
    fi
    echo "<==="
}

run_test(){
    tf_files="$1"
    if [ -z "$tf_files" ]; then
        help "terraform_validate"
        return 1
    fi

    if required; then
        terraform_validate $tf_files > output.json

        echo "===> output json:"
        cat output.json
        echo "<==="

        input_elasticsearch_log output.json
    fi

    if [ $check_error == 1 ]; then
        echo "===> Failed deploy. Valide os logs."
        return 1
    else
        echo "===> Success deploy."
    fi
}

run_test $1

echo "---"
echo "${GIT_BRANCH}"
