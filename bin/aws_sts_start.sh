#!/bin/sh

##
# Script para assume-role atraves de chaves fornecidas por environments
# Provider: AWS
# Author: DevOps team Arena
# Dependencies: awscli jq
##

set -e

unset AWS_SESSION_TOKEN
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_SESSION_TOKEN
export AWS_ARN_ROLE
export AWS_SECOND_ARN_ROLE

msg_error="Please set variable"

if [ -z "$AWS_ACCESS_KEY_ID" ]; then
    echo "$msg_error \$AWS_ACCESS_KEY_ID"
    exit
fi

if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
    echo "$msg_error \$AWS_SECRET_ACCESS_KEY"
    exit
fi

if [ -z "$AWS_ARN_ROLE" ]; then
    echo "$msg_error \$AWS_ARN_ROLE"
    exit
fi

run_first_sts(){
    first_sts=$(aws sts assume-role \
               --role-arn "${AWS_ARN_ROLE}" \
               --role-session-name awscli)

    export AWS_ACCESS_KEY_ID=$(echo $first_sts | jq .Credentials.AccessKeyId | xargs)
    export AWS_SECRET_ACCESS_KEY=$(echo $first_sts | jq .Credentials.SecretAccessKey | xargs)
    export AWS_SESSION_TOKEN=$(echo $first_sts | jq .Credentials.SessionToken | xargs)
}

run_second_sts(){
    if [ -z $AWS_SECOND_ARN_ROLE ]; then
        exit
    else
        run_first_sts

        second_sts=$(aws sts assume-role \
                    --role-arn ${AWS_SECOND_ARN_ROLE} \
                    --role-session-name awscli)

        export AWS_ACCESS_KEY_ID=$(echo $second_sts | jq .Credentials.AccessKeyId | xargs)
        export AWS_SECRET_ACCESS_KEY=$(echo $second_sts | jq .Credentials.SecretAccessKey | xargs)
        export AWS_SESSION_TOKEN=$(echo $second_sts | jq .Credentials.SessionToken | xargs)
    fi

    return
}

print_sts_keys(){
    echo export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    echo export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    echo export AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN
}

main(){
    if run_first_sts && [ -z $AWS_SECOND_ARN_ROLE ]; then
        print_sts_keys
    fi

    if run_second_sts; then
        print_sts_keys
    fi
}

main

