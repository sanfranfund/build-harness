#!/usr/bin/env bash

set -eu

export NAMESPACE=${3:-green}
export TERRAFORM_ARG=${1:-plan}
export TERRAFORM_WORKSPACE=${2:-default}
export BITBUCKET_USER=${BITBUCKET_USER:-user}
export BITBUCKET_PASSWORD=${BITBUCKET_PASSWORD:-pass}
export TIER=${TIER:-integration}
export REGION=${REGION:-us-east-1}

BITBUCKET_URL_API=https://api.bitbucket.org/2.0/repositories
TERRAFORM_BACKEND_CONF=backend-${REGION}-${TIER}.conf
TERRAFORM_GLOBAL_TFVARS=${REGION}-${TIER}


color="\e[32m"
error_color="\033[31m"


command_exists () {
    type "$1" > /dev/null ;
}


terraform_select_workspace(){
    for match in $(${TERRAFORM} workspace list) ; do
        if [ $match == ${TERRAFORM_WORKSPACE} ]; then
            echo -e "${color}==> Carregando workspace: ${TERRAFORM_WORKSPACE} \e[0m \n"
            ${TERRAFORM} workspace select ${TERRAFORM_WORKSPACE}
            return 0
        fi
    done

    return 1
}

terraform_new_workspace(){
    echo -e "${color}==> Workspace nao encontrado, criando workspace: ${TERRAFORM_WORKSPACE} \e[0m \n"
    ${TERRAFORM} workspace new ${TERRAFORM_WORKSPACE}
}


echo "===> Checando terraform:"
if command_exists terraform ; then
    echo "> $(terraform version)"
    export TERRAFORM=$(which terraform)
    else
    echo "> Inicializando terraform."
    GET_TERRAFORM_BIN=$(curl -s -S -O https://releases.hashicorp.com/terraform/0.12.7/terraform_0.12.7_linux_amd64.zip && unzip -o terraform_0.12.7_linux_amd64.zip -d /usr/local/bin && chmod +x /usr/local/bin/terraform)
    export TERRAFORM=$(which terraform)
fi



if [ -f ${TERRAFORM_GLOBAL_TFVARS} ] && [ -f ${TERRAFORM_BACKEND_CONF} ]; then
    echo "===> Removendo arquivos do terraform backend e variaveis globais existentes."
    rm -rf ${TERRAFORM_BACKEND_CONF} ${TERRAFORM_GLOBAL_TFVARS}
fi


echo "===> Atualizando variaveis globais:"
GET_BACKEND_TERRAFORM_CONF=$(curl -s -S --user ${BITBUCKET_USER}:${BITBUCKET_PASSWORD} -L -O ${BITBUCKET_URL_API}/sanfranfund/terraform-aws-arena/src/master/conf/${REGION}/${NAMESPACE}-${TIER}/backend/${TERRAFORM_BACKEND_CONF})
GET_GLOBAL_TFVARS_TERRAFORM=$(curl -s -S --user ${BITBUCKET_USER}:${BITBUCKET_PASSWORD} -L -O ${BITBUCKET_URL_API}/sanfranfund/terraform-aws-arena/src/master/conf/${REGION}/${NAMESPACE}-${TIER}/tfvars/${TERRAFORM_GLOBAL_TFVARS})
echo "OK"


${TERRAFORM} init -backend-config=${TERRAFORM_BACKEND_CONF} -reconfigure -no-color


if ! terraform_select_workspace; then
    terraform_new_workspace
fi


export $(cat ${TERRAFORM_GLOBAL_TFVARS})

if [ ${TERRAFORM_ARG} == "destroy" ]; then
    ${TERRAFORM} plan -destroy -out=.terraform.plan
    else
    ${TERRAFORM} ${TERRAFORM_ARG} -out=.terraform.plan
fi
