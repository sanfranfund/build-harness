#!/bin/sh

##
# Script para realizar o registro de uma nova task e atualizar.
# Provider: AWS
# Author: DevOps team Arena
# Dependencies: awscli jq
##

set -e

export AWS_REGION

start_msg_error="Please set variable"

if [ -z $AWS_REGION ]; then
    echo "${start_msg_error} AWS_REGION"
    exit 1
fi

help(){
    echo "Please use syntax:"
    echo "$0 -i your_image_name -n your_cluster_name -t your_task_name -s your_service_name -c your_container_name"
}

ecs_getoptions() {
    if [ "$#" -eq "0" ]; then
       help;
       exit 1;
    fi
    while getopts n:i:t:s:c: option; do
        case "${option}" in
            i) IMAGE=${OPTARG};;
            n) CLUSTER_NAME=${OPTARG};;
            t) TASK_FAMILY=${OPTARG};;
            s) SERVICE_NAME=${OPTARG};;
            c) CONTAINER_DEFINITION=${OPTARG};;
        esac
    done
}

command_exists () {
    type "$1" > /dev/null ;
}

ecs_register_task_definition(){
    if [ -z $TASK_FAMILY ]; then
        echo "Inform TASK_FAMILY"
        exit 1
    fi

    if [ -z $CONTAINER_DEFINITION ]; then
        echo "Inform CONTAINER_DEFINITION"
    fi

    if command_exists aws; then
        TASK_VERSION=$(aws ecs register-task-definition --family ${TASK_FAMILY} --container-definitions "${CONTAINER_DEFINITION}" --region ${AWS_REGION}| jq --raw-output '.taskDefinition.revision')
        echo "Task version registred: ${TASK_VERSION}"
        else
        echo "No command aws. Please install awscli https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-install.html"
        exit 1
    fi

    return
}

ecs_update_service() {
    if [ -z $CLUSTER_NAME ]; then
        echo "Inform CLUSTER_NAME"
        exit 1
    fi

    if [ -z $SERVICE_NAME ]; then
        echo "Inform SERVICE_NAME"
        exit 1
    fi

    if ecs_register_task_definition; then
        DEPLOYED=$(aws ecs update-service --cluster $CLUSTER_NAME --service $SERVICE_NAME --task-definition $TASK_FAMILY:$TASK_VERSION --region ${AWS_REGION})
        echo "Task version deployed: ${DEPLOYED}"
        else
        echo "Failed ecs register task"
        exit 1
    fi
}

ecs_run_update() {
    if ecs_register_task_definition ; then
        ecs_update_service
    fi
}

ecs_getoptions $@

ecs_run_update

exit